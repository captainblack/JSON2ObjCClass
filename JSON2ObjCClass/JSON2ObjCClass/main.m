//
//  main.m
//  JSON2ObjcClass
//
//  Created by Captain Black on 15-2-18.
//  Copyright (c) 2015年 aipai. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
